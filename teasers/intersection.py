"""
    Example code to compute the intersection of two line segments in 2D.
"""

from collections import namedtuple

Point2D = namedtuple( 'Point2D', [ 'x', 'y' ] )

def defaultForm( p1, p2 ):
    """ using the form ax+bx+c=0 return tuple (a, b, c) which represents the 2d line
        equation as defined from the two points p1 and p2. """
       
    y_delta = p2.y - p1.y
    x_delta = p2.x - p1.x
    
    if x_delta == 0.0 and y_delta == 0.0:
        return 0.0, 0.0, 0.0
    
    if abs( y_delta ) > abs( x_delta ):
        m = x_delta / y_delta
        # using x = my+c giving 1.0x -my -c = 0
        c = p1.x - ( m * p1.y )
        return 1.0, -m, -c
    else:
        m = y_delta / x_delta
        # using y = mx+c giving mx -1.0y +c = 0
        c = -( m * p1.x ) + p1.y
        return m, -1.0, c

def intersection( A1, A2, B1, B2 ):
    """ compute the intersection between two lines A and B, if one exists.
        line A is defined by points A1 and A2. line B is defined by points B1 and B2.
        returns a Point2D, or None. """

    Ax, Ay, Aw = defaultForm( A1, A2 )
    Bx, By, Bw = defaultForm( B1, B2 )
    
    # treat as a pair of homogeneous coordinates. compute the cross product
    crossprod = [ Ay*Bw - By*Aw, Bx*Aw - Ax*Bw, Ax*By - Bx*Ay ]
    w = crossprod[2]
    if abs(w) > 0:
        # normalise to 2d space
        ix = Point2D( crossprod[0] / w, crossprod[1] / w )
        return ix
    # zero w indicates no intersection
    
def _withinLimits( x, limit1, limit2 ):
    """ determine if x lies between limit1 and limit2 irrespective of order """
    return min( limit1, limit2 ) <= x <= max( limit1, limit2 )

def withinLimits( ix, pt1, pt2 ):
    """ check that point ix is within the ranges defined by square pt1->pt2 """
    return _withinLimits( ix.x, pt1.x, pt2.x ) and _withinLimits( ix.y, pt1.y, pt2.y )
   
def segmentIntersection( A1, A2, B1, B2 ):
    """ same as intersection() but only returns when the point lies within both line segments. """
    ix = intersection( A1, A2, B1, B2 )
    if ix and withinLimits( ix, A1, A2 ) and withinLimits( ix, B1, B2 ):
        return ix

def test():
    """ do some basic testing of the module features """
        
    A1 = Point2D( 1.0, 5.0 )    # y = 2x + 3
    A2 = Point2D( 5.0, 13.0 )   
    B1 = Point2D( 1.0, 6.5 )    # y = -0.5x + 7
    B2 = Point2D( 5.0, 4.5 )
    
    # we can easily check the above holds for:
    expected = Point2D( 1.6, 6.2 )  
    
    # demonstrate order does not matter:   
    assert segmentIntersection( A1, A2, B1, B2 ) == expected
    assert segmentIntersection( A2, A1, B1, B2 ) == expected
    assert segmentIntersection( A1, A2, B2, B1 ) == expected
    assert segmentIntersection( A2, A1, B2, B1 ) == expected
    assert segmentIntersection( B1, B2, A1, A2 ) == expected
    assert segmentIntersection( B2, B1, A1, A2 ) == expected
    assert segmentIntersection( B1, B2, A2, A1 ) == expected
    assert segmentIntersection( B2, B1, A2, A1 ) == expected    
    
    # demonstrate intersection outwith segment
    A1 = Point2D( 2.0, 7.0 )    # same equation but 1.6 < 2.0
    assert intersection( A1, A2, B1, B2 ) == expected
    assert segmentIntersection( A1, A2, B1, B2 ) != expected
    
    # demonstrate non-intersection
    A1 = Point2D( 0.0, 0.0 )    # y = 2x
    A2 = Point2D( 1.0, 2.0 )   
    B1 = Point2D( 0.0, 2.0 )    # y = 2x + 2
    B2 = Point2D( 1.0, 4.0 )
    assert intersection( A1, A2, B1, B2 ) is None
    
    # demonstrate vertical
    A1 = Point2D( 100.0, 0.0 )   # x = 100
    A2 = Point2D( 100.0, 1.0 )
    B1 = Point2D( 0.0, 0.0 )     # y = x
    B2 = Point2D( 1.0, 1.0 )
    
    assert intersection( A1, A2, B1, B2 ) == Point2D( 100.0, 100.0 )
    
    # demonstrate horizontal
    A1 = Point2D( 0.0, 100.0 )   # y = 100
    A2 = Point2D( 1.0, 100.0 )
    B1 = Point2D( 0.0, 10.0 )    # y = -4x + 10
    B2 = Point2D( 1.0, 6.0 )
    
    assert intersection( A1, A2, B1, B2 ) == Point2D( -22.5, 100.0 )
    
    # demonstrate both
    A1 = Point2D( -1000.0, 100.0 )    # x = -1000
    A2 = Point2D( -1000.0, 101.0 )
    B1 = Point2D( -100.0, -1000.0 )   # y = -1000
    B2 = Point2D( -101.0, -1000.0 )
    
    assert intersection( A1, A2, B1, B2 ) == Point2D( -1000.0, -1000.0 )
    