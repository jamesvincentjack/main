# README #

Welcome to my bitbucket repo - I upload simple python examples here and maybe useful utilities I've built. Start small and work your way up.

- see `git_help.md` for my quick git cheatsheet

### Directory: root ###

Unordered code examples.

### Directory: teasers ###

Example answers to interview-style questions - one file per question.

### Directory: unit_testing  ###

Examples and ideas on how to unit test. 

# In the future... #

concurrent.futures
