"""
Fun with Fibonacci - shows recursive vs iterative cache implementations
"""

class FibonacciCache( object ):
    """ calculate Fibonacci with the help of a cache. O(n) runtime. """
    
    def __init__( self ):
        self.cache = [ 0, 1 ]
    
    def sequenceItem( self, n ):
        """ return sequence item n from Fibonacci series.
            If it does not exist in cache, calculate and store the values.
        """
        if n < 0:
            return None
        # don't recurse and use stack; just iterate and store in the cache
        L = len( self.cache )
        while n >= L:
            nextValue = self.cache[ L - 1 ] + self.cache[ L - 2 ]
            self.cache.append( nextValue )
            L += 1
        return self.cache[ n ]

def recursiveFibonacci( n ):
    """ the classic recursive way to calculate Fibonacci numbers. O(2^n) runtime.
        best not to call this on n > 30 or so
    """
    if n < 0:
        return None
    if n < 2:
        return n
    return recursiveFibonacci( n - 1 ) + recursiveFibonacci( n - 2 )
    
if __name__ == "__main__":
    fib = FibonacciCache()
    for n in [ -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 2 ]:
        seqItem     = fib.sequenceItem( n )
        checkItem   = recursiveFibonacci( n )
        print 'n', n, ':', seqItem, 'check', checkItem, '. len is', len( fib.cache )
        
    # example output:
    #
    # n -1 : None check None . len is 2
    # n 0 : 0 check 0 . len is 2
    # n 1 : 1 check 1 . len is 2
    # n 2 : 1 check 1 . len is 3
    # n 3 : 2 check 2 . len is 4
    # n 4 : 3 check 3 . len is 5
    # n 5 : 5 check 5 . len is 6
    # n 6 : 8 check 8 . len is 7
    # n 7 : 13 check 13 . len is 8
    # n 8 : 21 check 21 . len is 9
    # n 9 : 34 check 34 . len is 10
    # n 10 : 55 check 55 . len is 11
    # n 2 : 1 check 1 . len is 11