"""
	Example to show how changing the MRO can be used instead for testing.
"""

from   collections import namedtuple
from   datetime import datetime
import logging
import unittest

Config = namedtuple( 'Config', [ 'topic', 'region', 'pair' ] )

CONFIGS = [ Config( 'INTERNAL', 'ASIA', 'EURUSD' ),
			Config( 'FEED1', 'US', 'GBPUSD' ) ]


class BlackBoxPublisher( object ):
	""" imported class from a third-party vendor, which you cannot see into. """

	def __init__( self ):
		logging.warn( 'BlackBoxPublisher: This should only happen in production.' )

	def publish_raw( self, encodedData ):
		logging.debug( 'BlackBoxPublisher: %s', encodedData )


class DataStreamPublisher( BlackBoxPublisher ):

	def __init__( self, topic ):
		super( DataStreamPublisher, self ).__init__()
		self.topic = topic
		self.encodePattern = '[%s]: %%s %%s' % ( self.topic, )

	def publish( self, data ):
		""" publish data as string on the topic """
		
		timestamp = datetime.utcnow().isoformat()
		preparedData = self.encodePattern % ( timestamp, data )
		self.publish_raw( preparedData )


class CurrencyPublisher( object ):

	def __init__( self, config, publisherClass ):
		self.config = config
		self.publisher = publisherClass( self.config.topic )

	def prepare( self, rate ):
		baseTick = ( self.config.pair, rate )
		return baseTick

	def publish( self, rate ):
		""" publish a currency rate to the stream """

		baseTick = self.prepare( rate )
		self.publisher.publish( baseTick )


class StaticDataEnrichment( object ):

	def prepare( self, rate ):
		""" perform some custom feed enrichment """

		# assumes you want to keep the original fuctionality
		baseTick = super( StaticDataEnrichment, self ).prepare( rate )

		if self.config.region in ( 'EMEA', 'ASIA' ):
			if self.config.topic in ( 'INTERNAL', ):
				return baseTick + ( 'EASTERN_PRIVATE', )
			return baseTick + ( 'EASTERN', )
		elif self.config.region in ( 'US', 'Canada' ):
			return baseTick + ( 'WESTERN', )
		return baseTick + ( 'UNKOWN', )


class EnrichedCurrencyPublisher( StaticDataEnrichment, CurrencyPublisher ):
	pass


class Driver( object ):

	def tickOnce( self, publisherClass ):
		logging.getLogger().setLevel( logging.DEBUG )

		publishers = []
		for config in CONFIGS:
			simplePublisher = CurrencyPublisher( config, publisherClass )
			fancyPublisher = EnrichedCurrencyPublisher( config, publisherClass )
			publishers.append( simplePublisher )
			publishers.append( fancyPublisher )

		# in some publish loop...
		rate = 1.2345
		for publisher in publishers:
			publisher.publish( rate )


"""
>>> Driver().tickOnce( DataStreamPublisher )

WARNING:root:BlackBoxPublisher: This should only happen in production.
WARNING:root:BlackBoxPublisher: This should only happen in production.
WARNING:root:BlackBoxPublisher: This should only happen in production.
WARNING:root:BlackBoxPublisher: This should only happen in production.
DEBUG:root:BlackBoxPublisher: [INTERNAL]: 2018-09-16T13:18:18.563000 ('EURUSD', 1.2345)
DEBUG:root:BlackBoxPublisher: [INTERNAL]: 2018-09-16T13:18:18.564000 ('EURUSD', 1.2345, 'EASTERN_PRIVATE')
DEBUG:root:BlackBoxPublisher: [FEED1]: 2018-09-16T13:18:18.564000 ('GBPUSD', 1.2345)
DEBUG:root:BlackBoxPublisher: [FEED1]: 2018-09-16T13:18:18.564000 ('GBPUSD', 1.2345, 'WESTERN')
"""


#--- Associated test file:

class BlackBoxPublisherProxy( BlackBoxPublisher ):

	def __init__( self, *args, **kwargs ):
		pass

	def publish_raw( self, *args, **kwargs ):
		logging.info( 'BlackBoxPublisherProxy: %s %s', args, kwargs )

class SafeDataStreamPublisher( DataStreamPublisher, BlackBoxPublisherProxy ):
	pass

class CurrencyPublisherTest( unittest.TestCase ):

	def test_run( self ):
		Driver().tickOnce( SafeDataStreamPublisher )


""" 
>>> unittest.main()

INFO:root:BlackBoxPublisherProxy: ("[INTERNAL]: 2018-09-16T13:18:41.684000 ('EURUSD', 1.2345)",) {}
INFO:root:BlackBoxPublisherProxy: ("[INTERNAL]: 2018-09-16T13:18:41.684000 ('EURUSD', 1.2345, 'EASTERN_PRIVATE')",) {}
INFO:root:BlackBoxPublisherProxy: ("[FEED1]: 2018-09-16T13:18:41.684000 ('GBPUSD', 1.2345)",) {}
INFO:root:BlackBoxPublisherProxy: ("[FEED1]: 2018-09-16T13:18:41.685000 ('GBPUSD', 1.2345, 'WESTERN')",) {}
.
----------------------------------------------------------------------
Ran 1 test in 0.001s
"""


""" 
So obviously MagicMock and patch are very useful and I use them in my unit tests where needed. However, the disturbing trend is to
use a patch from the test file and assume that it works without really checking. If the patch misses the target, normally the 
unittest works just fine, but it is really connecting to the real (hopefully UAT) environment. This can also happen when the patch 
is correct, but then someone goes and changes the driver (different import or whatever) and runs the tests, they work fine, and 
pushes the code. Oh dear. The unit test works on commit, but then overnight remote tests fail "for no reason" (i.e. when the 
service is down). Depending on your setup and the underlying code in BlackBoxPublisher, you may not be able to tell very easily 
whether the patch worked.

So I prefer to test using "Safe" substitutes that block the normal MRO. Whether this is really more robust or not, I am still to 
find out, but I hope it is more likely that the test will break when the underlying changes. It also forces you to remember the 
C3 linearization algorithm which is always a good thing.


Let's have a look at the Method Resolution Order of each:

DataStreamPublisher MRO:
 |      DataStreamPublisher
 |      BlackBoxPublisher
 |      __builtin__.object

When DataStreamPublisher calls super's __init__, that goes into BlackBoxPublisher and starts whatever black magic lies inside.


BlackBoxPublisherProxy MRO:
 |      BlackBoxPublisherProxy
 |      BlackBoxPublisher
 |      __builtin__.object

BlackBoxPublisherProxy's __init__ does not call super. In the test, we don't want it to call the real stuff.


SafeDataStreamPublisher MRO:
 |      SafeDataStreamPublisher
 |      DataStreamPublisher
 |      BlackBoxPublisherProxy
 |      BlackBoxPublisher
 |      __builtin__.object

Here, when DataStreamPublisher calls super's __init__, it goes to BlackBoxPublisherProxy, which just has 'pass' and so
BlackBoxPublisher's __init__ is never called.


The reason BlackBoxPublisher does not appear earlier in the MRO is down to how the C3 linearization algorithm works:

L(SafeDataStreamPublisher) := [SafeDataStreamPublisher] 
                              + merge( [DataStreamPublisher,BlackBoxPublisher], [BlackBoxPublisherProxy,BlackBoxPublisher], [DataStreamPublisher,BlackBoxPublisherProxy] )

	DataStreamPublisher is a good head. Remove DataStreamPublisher.

                           := [SafeDataStreamPublisher,DataStreamPublisher] + merge( [BlackBoxPublisher], [BlackBoxPublisherProxy,BlackBoxPublisher], [BlackBoxPublisherProxy] )

	BlackBoxPublisher is not a good head because it appears in a tail. 
	BlackBoxPublisherProxy is a good head. Remove BlackBoxPublisherProxy.

                           := [SafeDataStreamPublisher,DataStreamPublisher,BlackBoxPublisherProxy] + merge( [BlackBoxPublisher], [BlackBoxPublisher] )

	BlackBoxPublisher is a good head. Remove BlackBoxPublisher.

                           := [SafeDataStreamPublisher,DataStreamPublisher,BlackBoxPublisherProxy,BlackBoxPublisher]

When to call super().func and when to call self.func? 
 - It depends if you want to start from the beginning of the MRO or not.
 - super() simply calls "the next one in line" (Raymond Hettinger)

"""