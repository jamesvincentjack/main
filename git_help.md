# GIT command summary

This is my personal reference for git commands and a general memory aid

***

`git --version`
> version of git

`git help subcommand`
> get help on subcommand 

`git init`
> create a new git repo in current dir

> does not add any files into the index / staging area / cache

`git add file.py`
> add `file.py` into the index / staging area / cache and track it for changes

`git add --interactive`
> helps you decide which files to stage, untrack, etc

`git reset -- file.py`
> remove `file.py` from the index / staging area (stops tracking it)

`git status`
> summary of what's in the index / staging area

> shows files in the directory but not in the index (not yet added)

`export GIT_EDITOR=vi`
> tells git to use vi text editor when making commits (environment variable)

> or you can set core.editor param

```
git config --global user.name "Bilbo Baggins"
git config --global user.email "abc@xyz.com"
```
> sets author name and email in $HOME/.getconfig for all your commits (all repos)

> remove --global and it becomes specific to the repo you're in

`git config -l`
> list config values for git

`git config --unset --global user.email`
> remove config value

`git commit`
> commits changes in the index / staging area into the local repository

> uses the environment's editor

git never commits with an empty log message
if you're aready in vi, remove all text and quit to stop the commit

`git commit -m "summary of commit"`
> commit changes into the local repo with the commit summary specified

`git commit --interactive`
> decide interactively what to commit, quite useful

`git commit -A`    or `-all`
> ALL unstaged files are staged and then committed, including already staged files

> includes rm, mv, etc

> does NOT include new directories with no tracked files

`git log`
> show commit history

`git log --follow file.py`
> show commit history for `file.py`, follow for mv history

`gitk --all`
> visual commit graph with all nodes in the DAG

`git config --global alias.show-graph 'log --graph --decorate=full --source --pretty'`
> creates an alias (shortcut) - when you type `git show-graph` it runs the longer command

`git show <commit hash>`
> shows info and diff on the commit (by hash)

`git show-branch --more=10`
> show one-line summaries for current branch (max 10)

`git branch`
> shows what branch you are on

`git diff <old commit hash> <newer commit hash>`
> shows file diffs between the two commits

> `-` is the old lines, `+` the new lines

`git diff`
> shows changes in working directory not staged

`git diff --cached`
> shows staged changes versus local repo

`git rm file.py`
> remove `file.py` from the index and the local directory

> does not work if file was never in the index/repo

> checks that working file matches index

`git rm --cached file.py`        (not advised)
> remove `file.py` from staging. working directory file remains

> does not check if working file has changes

`git checkout -- file.py`
> get the file back from the index (puts in local directory)

`git checkout HEAD -- file.py`
> get the file back as referenced by HEAD (puts in local directory)

`git mv file.py file2.py`
> rename `file.py` to `file2.py` in the index (and local directory)

`git clone repo1 repo2`
> clones a copy of `repo1` and puts it in `repo2` (makes dir)

`git ls-files -s`      or `--stage`
> list files in the index and their object hash

`git rev-parse xyzabc`
> get the full object hash code starting with the first few characters

`git cat-file -p xyzabc`
> get the contents of the object (file) from the object hash code (from the index)

> can be short hash id

`find .git/objects`
> just useful to list objects in the object store

`git hash-object file.py`
> generate the SHA1 hash of the file and does nothing else

`git write-tree`
> write a tree for the staged files into the object store (can be viewed with cat-file)

> because the hash is based on the contents, running twice gives the same hash id

`git tag -m "Tag version 0.5" "v0.5" 22a8`
> create an annotated (heavy) tag to a commit hash

> `git show v0.5` shows the details

> tags live in e.g. `refs/tags/v0.5`

`.gitignore`
> add file names to exclude from untracked files in git status

> `.gitifnore` should be a tracked file (and so copied during `git clone`)

> < do you want cloned repo's do exclude the same files?

> < if not: use `.git/info/exclude`

> supports globbing on files: `/build/*.o` but not across directories

> `!` at the start inverts the match (previously excluded files are included)

> `#` at the start is a comment

> you can have `.gitignore` per directory (takes precedence)

> 2nd lowest precedence: `.git/info/exclude`

> lowest precedence: `core.excludefile` param

> highgest precendence: command line

`HEAD` always points to the most recent commit on the current branch

`ORIG_HEAD` is the previous `HEAD`

`FETCH_HEAD` is the head from remote repo just fetched

`MERGE_HEAD` is the commit being merged into `HEAD`

After a commit, `HEAD` points to the new commit, and the old commit is a parent

After a merge, the new commit would have (at least) 2 parent commits
> `HEAD^1 is the first parent, `HEAD^2` is the second parent, etc

> `HEAD~1` is the previous commit (on first parent, if there is more than one)

> `HEAD~2` is previous previous commit

> `HEAD~~~` is shorthand for HEAD~3

> get the absolute commit hash with e.g. `git rev-parse HEAD~2`

> `git log HEAD~2`, `git log master`, etc

> `git show HEAD~2` etc

`git log --pretty=short master~12..master~10`  -->  OLDER`..`NEWER
> git commits between master~12 and master~10
> or alternatively "commits reachable from NEWER minus commits reachable from OLDER"

`git log topic..master`
> commits reachable from branch master not reachable from branch topic (and vice versa)
> either arg may be left out and `HEAD` is assumed

`git log A...B`
> symmetric difference: reachable from A OR B but not both

`git log ^dev ^topic ^bugfix master`
> commits in master, not in any of dev, topic, or bugfix

`git log hash`
> show me the commits reachable from the commit hash (i.e. parents of the commit)

> master~12 is not shown (exclusive) 

`git log -1 -p <hash>`
> get the patch (diffs) introduced by the commit

`git show HEAD~2:file.py`
> show `file.py` from 2 commits back from `HEAD`

## How to use `git bisect`

Start with a clean working directory, or else you will lose changes.

`git bisect start`

enters a detached head state, then

`git bisect bad`

is saying this current version (`HEAD`) is bad (has a bug). Then tell it which is good:

`git bisect good v2.6.27`

and it narrows down the list of possible commits: it bisects the entire lot and asks you a question on each round. From the current working directory, work out if it contains the bug or not...

`git bisect good`

or

`git bisect bad`

it will narrow down the commits for you.

`git bisect log`
> gives a list of your answers and the commits

to start over: `git bisect replay` and give it the log

`git bisect visualize` will present the remaining commits graphically

`git bisect visualize --pretty=oneline` same but using console

EXIT bisect mode: `git bisect reset` and you get back to your original branch

## How to use `git blame`

Tells you who last changed each line in a file and the related commit

`git blame -L 35, file.py`

## How to use pickaxe

`git log -Spandas --pretty=oneline file.py`

Searches commit _diffs_ on `file.py` for the string `pandas`.  May not work if the number of adds and subtracts were exactly the same.


## Branches vs Tags

tags are labels for individual commits that are important in some way e.g. __v1.0 initial release__, but a branch is a line of development and has many commits on it - a moving target. easy!

Branch names should be organised! e.g. bugs/JIRA-1034 and bugs/JIRA-1089 would be two usefully named branches, and the slash provides wildcard utility: `git show-branch 'bug/*'`

Branches names: dont include funny characters, especially dot, asterisk, tilde, caret, colon, question mark, etc. dash is okay when not at the start of the name.

## Branching

```
git branch branch-name
git branch branch-name commit-hash
```
> will take current commit from head or from the `commit-hash`

> does not change your working directory

then switch into:

`git checkout branch-name`











 



