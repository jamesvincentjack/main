"""
    Monty Hall - monte carlo!
"""

from   collections import Counter
import random


def playGame( switch ):

    # arrange 1 prize and 2 goats
    doors = [ 'Car', 'Goat1', 'Goat2' ]
    random.shuffle( doors )

    # pick a random door
    firstChoice = random.choice( doors )

    # remove choice
    doors.remove( firstChoice )

    # pick a random Goat
    goat = random.choice( doors )
    while goat == 'Car':
        goat = random.choice( doors )

    # remove goat
    doors.remove( goat )

    # conduct experiment!
    if switch:
        # only 1 item remains
        return doors[ 0 ] # 2/3 chance is available

    return firstChoice # still 1/3 chance of car

def main():

    # experiment 1 - always switch your choice
    switch = Counter()
    for _ in range( 1_000_000 ):
        switch[ playGame( True ) ] += 1

    # experiment 2 - never switch your choice
    stay = Counter()
    for _ in range( 1_000_000 ):
        stay[ playGame( False ) ] += 1

    print(switch)
    print(stay)

main()
